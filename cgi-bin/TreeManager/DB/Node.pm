package TreeManager::DB::Node;

## package contains functions to work with DB
## all SQL queries should be under folder /DB

use strict;

use TreeManager::Node;

use DBI;

## create a new connection to DB. 
## for a real project connection cache should be implemented
## connections should be shared between requests.
sub get_dbh {
    my $dbh = DBI->connect ("dbi:mysql:tree_manager:localhost", "root", "");

}

## add a new child to tree under passed node_id
sub add_child {
    my $class     = shift;
    my %args      = @_;

    my $dbh = get_dbh();

    my $parent_id = $args{'parent_id'};
    return unless $parent_id =~ /^\d+$/;

    ## trying to find level / depth of "parent" node.
    ## if level is empty, parent id is wrong and we return with error.
    my $parent_node = TreeManager::Node->new ( id => $parent_id );
    my $parent_level = $parent_node->level;
    return unless defined $parent_level;

    my $sql = "INSERT INTO nodes (name) VALUES ('name')";
    my $res = $dbh->do ($sql);
    my $node_id = $dbh->last_insert_id (undef, undef, "nodes", "id");

    my $sql = "
        INSERT INTO nodes_path (ancestor, descendant, level)
            SELECT ancestor, ?, ?
                FROM nodes_path
                WHERE descendant = ?
            UNION ALL
            SELECT ?, ?, ?
    ";
    $dbh->do ($sql, undef, $node_id, $parent_level + 1, $parent_id, $node_id, $node_id, $parent_level + 1);

    return $node_id;
}

## returning all nodes. only SQL query and return. 
## all parsing, grep'ing, map'ing will be done in caller function
sub get_all_nodes {
    my $dbh = get_dbh();

    my $nodes = $dbh->selectall_arrayref ("SELECT * FROM nodes", { Slice => {}});

    my %all_nodes = ();
    foreach my $node (@$nodes) {
        my $node_obj = TreeManager::Node->new (%$node);
        $node_obj->parent_id();

        my $node_level = $dbh->selectcol_arrayref ("SELECT level FROM nodes_path WHERE descendant = ? LIMIT 1", undef, $node->{'id'});

        push @{$all_nodes{$node_level->[0]}}, $node_obj;
    }

    return \%all_nodes;
}

sub get_level {
    my $class = shift;
    my %args  = @_;

    my $node_id = $args{'id'};
    return 0 if $node_id == 0;

    my $dbh = get_dbh();
    my $level = $dbh->selectcol_arrayref("
        SELECT level
            FROM nodes_path
            WHERE ancestor = ?
                AND descendant = ?",
        undef, $node_id, $node_id
    ) || [];

    return $level->[0];
}

sub get_parent_id {
    my $class = shift;
    my %args  = @_;

    my $node_id = $args{'id'};

    my $dbh = get_dbh();
    my $parent_id = $dbh->selectcol_arrayref("
        SELECT np1.ancestor
            FROM nodes_path np1,
                 nodes_path np2
            WHERE np1.level = np2.level - 1
                AND np1.ancestor = np2.ancestor
                AND np2.ancestor != np2.descendant
                AND np1.ancestor = np1.descendant
                AND np2.descendant = ?",
        undef, $node_id
    ) || [];

    return $parent_id->[0];
}

1;
