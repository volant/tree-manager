package TreeManager::Node;

use strict;

use TreeManager::DB::Node;

## package implements methods for class TreeManager::Node

sub new {
    my $class = shift;
    my %args  = @_;

    my $self = \%args;
    $self->{'nid'} = delete $self->{'id'};

    return bless $self, $class;
}

sub set_nodeid {
    my $self    = shift;
    my $node_id = shift;

    $self->{'nid'} = $node_id;

    return 1;
}

sub get_nodeid {
    my $self = shift;

    return $self->{'nid'};
}

## 'name' means any node's property(ies)
sub set_node_name {
    my $self = shift;
    my $name = shift;

    $self->{'name'} = $name;

    return 1;
}

sub get_node_name {
    my $self = shift;

    return $self->{'name'};
}

sub add_child {
    my $self      = shift;
    my $parent_id = shift;

    my $new_id = TreeManager::DB::Node->add_child(
        parent_id  => $parent_id,
    );

    return unless $new_id;

    ## I can return just new ID 
    ## but in future a created object could be needed in caller
    my $child = __PACKAGE__->new( id => $new_id );

    return $child;
}

sub level {
    my $self = shift;

    my $level = TreeManager::DB::Node->get_level(
        id  => $self->get_nodeid,
    );

    return $level;
}

sub parent_id {
    my $self = shift;

    ## if we call 'parent_id' more than one time
    ## return ID from local cache instead send query to DB
    return $self->{'parent_id'}
        if $self->{'parent_id'};

    my $parent_id = TreeManager::DB::Node->get_parent_id(
        id => $self->get_nodeid,
    );

    ## store in object (local cache) for future use
    $self->{'parent_id'} = $parent_id;

    return $parent_id;
}

"Node";
