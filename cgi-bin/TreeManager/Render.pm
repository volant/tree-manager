package TreeManager::Render;

use strict;

use HTML::Template::Pro;

sub render_page {
    my $class = shift;
    my $error = shift;

    my $all_nodes = TreeManager::DB::Node->get_all_nodes();

    my $template = HTML::Template::Pro->new (
        filename          => "templates/tree_manager.tmpl",
        loop_context_vars => 1,
    );

    my $tmpl_nodes = [];
    foreach my $level (sort { $a <=> $b } keys %$all_nodes) {
        my $nodes = $all_nodes->{$level};
        my $nodes_per_parent = {};
        foreach my $node (@$nodes) {
            push @{$nodes_per_parent->{$node->parent_id}}, $node;
        }

        push @$tmpl_nodes, {
            level => $level,
            nodes => [
                map {
                    {
                        parent => $_,
                        nodes  => $nodes_per_parent->{$_},
                    }
                } sort { $a <=> $b } keys %$nodes_per_parent,
            ],
        };
    }

    $template->param (
        nodes_list => $tmpl_nodes,
        error      => $error,
    );

    return $template->output;
}

1;
