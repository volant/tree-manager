package TreeManager::Main;

use strict;

use Apache2::RequestUtil;
use Apache2::RequestRec;
use Apache2::Request;
use Apache2::Const -compile => qw/REDIRECT/;

use TreeManager::Node;
use TreeManager::Render;

sub handler {

    my $r = Apache2::RequestUtil->request;

    my $uri = $r->uri;
    return Apache2::Const::OK()
        if $uri eq "/favicon.ico";

    my $error = "";

    my $req = Apache2::Request->new($r);
    my $to_pid = $req->param("PID");
    if (defined $to_pid) {
        my $node = TreeManager::Node->add_child($to_pid);
        unless ($node) {
            $error = "Can't add a new node to PID $to_pid";
        }
    }

    $r->content_type( "text/html" );

    my $html = TreeManager::Render->render_page($error);
    print $html;

    return Apache2::Const::OK();

}

!0;
